package PageObjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.GenericFunction;

public class FlightsInfo {

	WebDriver driver;
	GenericFunction generic;

	public FlightsInfo(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
	}

	By flights = By.xpath("//span[@class='airways-name ']");
	By prices = By.xpath("//span[@class='actual-price']");
	static HashMap<String, ArrayList<Integer>> val = new HashMap<String, ArrayList<Integer>>();

	public int getTotalNumberOfFlights() {

			generic.scroll();

			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(flights));

			List<WebElement> flightNames = driver.findElements(flights);
			List<WebElement> flightPrices = driver.findElements(prices);

			val=generic.makeKeyValuePairs(flightNames,flightPrices);

		
		return flightNames.size();
	}

	public int getFlightsNumberOf(String flightName) {
		ArrayList<Integer> al = val.get(flightName);
		return al.size();
	}

	public int getMaximumFairOf(String flightName) {
		ArrayList<Integer> al = val.get(flightName);
		Collections.sort(al);
		return al.get(al.size() - 1);
	}

	public int getMinimumFairOf(String flightName) {
		ArrayList<Integer> al = val.get(flightName);
		Collections.sort(al);
		return al.get(0);
	}
}
