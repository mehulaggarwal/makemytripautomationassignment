package Utilities;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class GenericFunction {

	WebDriver driver;
	String browserType;

	public GenericFunction(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public WebDriver StartDriver(String browserType) {
		this.browserType = browserType;
		if (browserType.toLowerCase().equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			this.driver = new ChromeDriver();

		} else if (browserType.toLowerCase().equals("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			this.driver = new FirefoxDriver();
		}
		return driver;
	}

	public boolean isVisible(By locator) {
		return driver.findElement(locator).isDisplayed();
	}

	public void Click(By locator) {
		driver.findElement(locator).click();
	}

	public void Fill_Text(By locator, String value) {
		WebElement element = driver.findElement(locator);
		element.sendKeys(value);
	}
	
	public int convertToInteger(String str) {
		int ans=0;
		for(int i=0;i<str.length();i++) {
			if(str.charAt(i)!=',') {
				ans=ans*10+str.charAt(i)-'0';
			}
		}
		return ans;
	}
	
	public void scroll() {
		System.out.println("Please wait while the page is scrolling. Might Take some time");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		long lastHeight = (Long) js.executeScript("return document.body.scrollHeight");

		while (true) {
			js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			long newHeight = (Long) js.executeScript("return document.body.scrollHeight");
			if (newHeight == lastHeight) {
				break;
			}
			lastHeight = newHeight;
		}
	}
	
	public HashMap<String, ArrayList<Integer> > makeKeyValuePairs(List<WebElement> elements1,List<WebElement> elements2){
		HashMap<String, ArrayList<Integer>> val = new HashMap<String, ArrayList<Integer>>();
		for (int i = 0; i < elements1.size(); i++) {
			WebElement element1 = elements1.get(i);
			WebElement element2 = elements2.get(i);
			String flightName = element1.getText();
			String[] price = element2.getText().split(" ");

			if (!val.containsKey(flightName)) {
				ArrayList<Integer> al = new ArrayList<Integer>();
				al.add(this.convertToInteger(price[1]));
				val.put(flightName, al);
			} else {
				ArrayList<Integer> al = val.get(flightName);
				al.add(this.convertToInteger(price[1]));
				val.put(flightName, al);
			}

		}
		return val;
	}
	

}
