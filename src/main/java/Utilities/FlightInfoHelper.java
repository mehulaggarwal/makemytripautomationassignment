package Utilities;

import org.openqa.selenium.WebDriver;

import PageObjects.FlightsInfo;

public class FlightInfoHelper {

	WebDriver driver;
	GenericFunction generic;
	FlightsInfo flightsInfo;

	public FlightInfoHelper(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		flightsInfo = new FlightsInfo(driver, generic);
	}

	public void getTotalNumberOfFlights() {

		System.out.println(flightsInfo.getTotalNumberOfFlights());
	}

	public void getFlightsNumberOf(String flightName) {

		System.out.println(flightsInfo.getFlightsNumberOf(flightName));
	}

	public void getMaximumFairOf(String flightName) {

		System.out.println(flightsInfo.getMaximumFairOf(flightName));
	}

	public void getMinimumFairOf(String flightName) {

		System.out.println(flightsInfo.getMinimumFairOf(flightName));
	}

}
