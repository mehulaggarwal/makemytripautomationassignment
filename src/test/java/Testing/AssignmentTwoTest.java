package Testing;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Config.ConfigFileReader;
import Utilities.BookFlightHelper;
import Utilities.FlightInfoHelper;
import Utilities.GenericFunction;

public class AssignmentTwoTest {
	
	String URL;
	String browserType;
	GenericFunction generic;
	WebDriver driver;
	BookFlightHelper bookFlightHelper;
	FlightInfoHelper flightsinfo;
	
	@BeforeTest
	public void beforeTest() {
		URL=ConfigFileReader.getConfigValue("url");
		browserType = ConfigFileReader.getConfigValue("browser");
		generic=new GenericFunction(driver);
		driver=generic.StartDriver(browserType);
		driver.manage().window().maximize();
		bookFlightHelper=new BookFlightHelper(driver, generic);
		flightsinfo =new FlightInfoHelper(driver, generic);
	}
	
	@Test
	public void test() {
		driver.get(URL);
		bookFlightHelper.bookTicket();
		flightsinfo.getTotalNumberOfFlights();
		flightsinfo.getFlightsNumberOf("IndiGo");
		flightsinfo.getMaximumFairOf("IndiGo");
		flightsinfo.getMinimumFairOf("IndiGo");
	}

}
