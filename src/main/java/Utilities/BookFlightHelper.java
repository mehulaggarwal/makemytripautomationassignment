package Utilities;

import org.openqa.selenium.WebDriver;

import PageObjects.BookFlight;

public class BookFlightHelper {
	
	WebDriver driver;
	GenericFunction generic;
	

	public BookFlightHelper(WebDriver driver,GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic=generic;
	}
	
	public void bookTicket() {
		BookFlight bookFlight=new BookFlight(driver, generic);
		bookFlight.fromCity("Delhi");
		bookFlight.toCity("Pune");
		bookFlight.selectDepartureDate();
		bookFlight.clickSearchButton();
	}

}
