package PageObjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.GenericFunction;

public class BookFlight {

	WebDriver driver;
	GenericFunction generic;

	public BookFlight(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;

	}

	By fromCity = By.xpath("//input[@id='fromCity']");
	By toCity = By.xpath("//input[@placeholder='To']");
	By selectFromCity = By.xpath("//ul[@role='listbox']/li/div/div/p[contains(.,'Delhi, India')]");
	By selectToCity = By.xpath("//ul[@role='listbox']/li/div/div/p[contains(.,'Pune, India')]");
	By selectDepartureDate = By.xpath("//div[@aria-label='Wed Jan 29 2020']");
	By searchButton = By.xpath("//a[@class='primaryBtn font24 latoBlack widgetSearchBtn '][contains(.,'Search')]");

	public void fromCity(String city) {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(fromCity));
		generic.Fill_Text(fromCity, city);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		generic.Click(selectFromCity);
	}

	public void toCity(String city) {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(toCity));
		
		generic.Fill_Text(toCity, city);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		
			e.printStackTrace();
		}
		generic.Click(selectToCity);
	}

	public void selectDepartureDate() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(selectDepartureDate));
		generic.Click(selectDepartureDate);

	}

	public void clickSearchButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(searchButton));
		generic.Click(searchButton);
	}
}
